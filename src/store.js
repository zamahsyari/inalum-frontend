import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    activeUser: {
      email: "",
      token: ""
    },
    activeGraph: {
      chartType: "none",
      title: "",
      data: []
    },
    activeData: {},
    showDetail: false,
    showPopup: false,
    baseURL: "http://localhost:8000",
    refreshData: false,
    showAttachment: false
  },
  mutations: {
    updateToken(state, value) {
      state.activeUser.token = value;
      localStorage.setItem("token", value);
    },
    updateEmail(state, value) {
      state.activeUser.email = value;
      localStorage.setItem("email", value);
    },
    setActiveGraph(state, activeGraph) {
      state.activeGraph = activeGraph;
    },
    setChartData(state, data) {
      state.activeGraph.data = data;
    },
    setShowDetail(state, value) {
      state.showDetail = value;
    },
    setShowPopup(state, value) {
      state.showPopup = value;
    },
    setActiveData(state, value) {
      state.activeData = value;
    },
    setRefreshData(state, value) {
      state.refreshData = value;
    },
    setShowAttachment(state, value) {
      state.showAttachment = value;
    }
  },
  getters: {
    getToken: state => {
      if (state.activeUser.token === "") {
        return localStorage.getItem("token");
      } else {
        return state.activeUser.token;
      }
    },
    getEmail: state => {
      if (state.activeUser.email === "") {
        return localStorage.getItem("email");
      } else {
        return state.activeUser.email;
      }
    },
    getChartType: state => {
      return state.activeGraph.chartType;
    },
    getChartData: state => {
      return state.activeGraph.data;
    },
    getChartTitle: state => {
      return state.activeGraph.title;
    },
    getShowDetail: state => {
      return state.showDetail;
    },
    getShowPopup: state => {
      return state.showPopup;
    },
    getBaseURL: state => {
      return state.baseURL;
    },
    getActiveData: state => {
      return state.activeData;
    },
    getRefreshData: state => {
      return state.refreshData;
    },
    getShowAttachment: state => {
      return state.showAttachment;
    }
  }
});
