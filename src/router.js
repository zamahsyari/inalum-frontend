import Vue from "vue";
import Router from "vue-router";
// import Home from './views/Home.vue'
import Login from "./views/Login.vue";
import Dashboard from "./views/Dashboard.vue";
import Detail from "./views/Detail.vue";
import Admin from "./views/Admin.vue";
import AdminChartContentList from "./components/AdminChartContentList.vue";
import AdminChartContentAdd from "./components/AdminChartContentAdd.vue";
import AdminChartContentEdit from "./components/AdminChartContentEdit.vue";

// import auth from './middleware/auth'
import store from "./store";
Vue.use(Router);

export default new Router({
  mode: "history",
  routes: [
    {
      path: "/",
      name: "home",
      redirect: "/dashboard"
    },
    {
      path: "/about",
      name: "about",
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () =>
        import(/* webpackChunkName: "about" */ "./views/About.vue")
    },
    {
      path: "/login",
      name: "login",
      component: Login
    },
    {
      path: "/dashboard",
      name: "dashboard",
      component: Dashboard,
      beforeEnter: (to, from, next) => {
        if (store.getters.getToken === "") {
          next("/login");
        }
        next();
      }
    },
    {
      path: "/detail",
      name: "detail",
      component: Detail,
      beforeEnter: (to, from, next) => {
        if (store.getters.getToken === "") {
          next("/login");
        }
        next();
      }
    },
    {
      path: "/admin",
      name: "admin",
      component: Admin,
      redirect: "/admin/penerimaan/list",
      children: [
        {
          path: ":title/list",
          component: AdminChartContentList
        },
        {
          path: ":title/add",
          component: AdminChartContentAdd
        },
        {
          path: ":title/edit",
          component: AdminChartContentEdit
        }
      ],
      beforeEnter: (to, from, next) => {
        if (store.getters.getToken === "") {
          next("/login");
        }
        next();
      }
    }
  ]
});
