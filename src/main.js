import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import { library } from "@fortawesome/fontawesome-svg-core";
import {
  faArrowLeft,
  faDownload,
  faTimes,
  faChartBar,
  faTable,
  faUpload,
  faPlus,
  faEdit,
  faPaperclip
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";
import Axios from "axios";

library.add(faArrowLeft);
library.add(faDownload);
library.add(faTimes);
library.add(faChartBar);
library.add(faTable);
library.add(faUpload);
library.add(faPlus);
library.add(faEdit);
library.add(faPaperclip);

Vue.component("font-awesome-icon", FontAwesomeIcon);

Vue.config.productionTip = false;

Vue.mixin({
  methods: {
    getChartApi(entity, title, redirect) {
      var self = this;
      const options = {
        method: "GET",
        headers: {
          token: this.$store.getters.getToken
        },
        params: {
          judul: title
        },
        url: this.$store.getters.getBaseURL + "/" + entity
      };
      return Axios(options)
        .then(response => {
          let data = response.data.data;
          return self.sortByKey(data, "tgl", "asc");
        })
        .catch(error => {
          if (error.response.status === 401 && redirect === true) {
            window.location.href = "/login";
          }
          console.log(error);
        });
    },
    getIssuingApi(endpoint) {
      var self = this;
      const options = {
        method: "GET",
        headers: {
          token: this.$store.getters.getToken
        },
        url: this.$store.getters.getBaseURL + "/" + endpoint
      };
      return Axios(options)
        .then(response => {
          let data = response.data.data;
          return self.sortByKey(data, "tgl", "asc");
        })
        .catch(error => {
          console.log(error.response);
        });
    },
    submitApi(entity, data) {
      const options = {
        method: "POST",
        headers: {
          token: this.$store.getters.getToken
        },
        data: data,
        url: this.$store.getters.getBaseURL + "/" + entity
      };
      return Axios(options)
        .then(response => {
          console.log(response);
        })
        .catch(error => {
          if (error.response.status === 401) {
            window.location.href = "/login";
          }
          console.log(error.response);
        });
    },
    updateApi(entity, data, id) {
      const options = {
        method: "POST",
        headers: {
          token: this.$store.getters.getToken
        },
        data: data,
        url: this.$store.getters.getBaseURL + "/" + entity + "/" + id
      };
      return Axios(options)
        .then(response => {
          console.log(response);
        })
        .catch(error => {
          if (error.response.status === 401) {
            window.location.href = "/login";
          }
          console.log(error.response);
        });
    },
    submitUploadApi(entity, data) {
      const options = {
        method: "POST",
        headers: {
          token: this.$store.getters.getToken,
          "Content-Type": "multipart/form-data"
        },
        data: data,
        url: this.$store.getters.getBaseURL + "/" + entity
      };
      return Axios(options)
        .then(response => {
          console.log(response);
        })
        .catch(error => {
          if (error.response.status === 401) {
            window.location.href = "/login";
          }
          console.log(error.response);
        });
    },
    updateUploadApi(entity, data, id) {
      const options = {
        method: "POST",
        headers: {
          token: this.$store.getters.getToken,
          "Content-Type": "application/x-www-form-urlencoded"
        },
        data: data,
        url: this.$store.getters.getBaseURL + "/" + entity + "/" + id
      };
      return Axios(options)
        .then(response => {
          console.log(response);
        })
        .catch(error => {
          if (error.response.status === 401) {
            window.location.href = "/login";
          }
          console.log(error.response);
        });
    },
    uploadApi(entity, data) {
      const options = {
        method: "POST",
        headers: {
          token: this.$store.getters.getToken,
          "Content-Type": "multipart/form-data"
        },
        data: data,
        url: this.$store.getters.getBaseURL + "/" + entity
      };
      return Axios(options)
        .then(response => {
          console.log(response);
        })
        .catch(error => {
          if (error.response.status === 401) {
            window.location.href = "/login";
          }
          console.log(error.response);
        });
    },
    deleteChartApi(entity, id) {
      const options = {
        method: "DELETE",
        headers: {
          token: this.$store.getters.getToken
        },
        url: this.$store.getters.getBaseURL + "/" + entity + "/" + id
      };
      return Axios(options)
        .then(response => {
          console.log(response);
        })
        .catch(error => {
          if (error.response.status === 401) {
            window.location.href = "/login";
          }
          console.log(error.response);
        });
    },
    getChartByIdApi(entity, id) {
      const options = {
        method: "GET",
        headers: {
          token: this.$store.getters.getToken
        },
        url: this.$store.getters.getBaseURL + "/" + entity + "/" + id
      };
      return Axios(options)
        .then(response => {
          return response.data.data;
        })
        .catch(error => {
          console.log(error.response);
        });
    },
    getAttachmentByIdApi(entity, id, filename) {
      const options = {
        method: "GET",
        headers: {
          token: this.$store.getters.getToken
        },
        responseType: "blob",
        url: this.$store.getters.getBaseURL + "/download/" + entity + "/" + id
      };
      return Axios(options)
        .then(async response => {
          const url = window.URL.createObjectURL(new Blob([response.data]), {
            type: response.data.type
          });
          const link = document.createElement("a");

          link.href = url;
          link.setAttribute("download", filename);
          document.body.appendChild(link);
          link.click();
        })
        .catch(error => {
          console.log(error.response);
        });
    },
    sortByKey(array, key, ascdesc = "asc") {
      return array.sort((a, b) => {
        let x = a[key];
        let y = b[key];
        if (ascdesc === "asc") {
          return x < y ? -1 : x > y ? 1 : 0;
        } else {
          return x > y ? -1 : x < y ? 1 : 0;
        }
      });
    },
    filterByKey(key, value, data) {
      let res = [];
      for (let i = 0; i < data.length; i++) {
        if (data[i][key] === value) {
          res.push(data[i]);
        }
      }
      return res;
    },
    parseDate(ISODate) {
      let d = new Date(ISODate);
      let months = [
        "Jan",
        "Feb",
        "Mar",
        "Apr",
        "Mei",
        "Jun",
        "Jul",
        "Agu",
        "Sep",
        "Okt",
        "Nov",
        "Des"
      ];
      return d.getDate() + " " + months[d.getMonth()] + " " + d.getFullYear();
    },
    getAgenda(tgl) {
      var self = this;
      const options = {
        method: "GET",
        headers: {
          token: this.$store.getters.getToken
        },
        params: {
          tgl: tgl
        },
        url: this.$store.getters.getBaseURL + "/agenda"
      };
      return Axios(options)
        .then(response => {
          let data = response.data.data;
          return self.sortByKey(data, "tgl", "asc");
        })
        .catch(error => {
          console.log(error.response);
        });
    },
    getPatrol(tgl) {
      var self = this;
      const options = {
        method: "GET",
        headers: {
          token: this.$store.getters.getToken
        },
        params: {
          tgl: tgl
        },
        url: this.$store.getters.getBaseURL + "/patrol"
      };
      return Axios(options)
        .then(response => {
          let data = response.data.data;
          return self.sortByKey(data, "tgl", "asc");
        })
        .catch(error => {
          console.log(error.response);
        });
    },
    getAward() {
      var self = this;
      const options = {
        method: "GET",
        headers: {
          token: this.$store.getters.getToken
        },
        params: {
          aktif: true
        },
        url: this.$store.getters.getBaseURL + "/award"
      };
      return Axios(options)
        .then(response => {
          let data = response.data.data;
          return self.sortByKey(data, "tgl", "asc");
        })
        .catch(error => {
          console.log(error.response);
        });
    },
    getImprovement(tgl) {
      var self = this;
      const options = {
        method: "GET",
        headers: {
          token: this.$store.getters.getToken
        },
        params: {
          tgl: tgl
        },
        url: this.$store.getters.getBaseURL + "/improvement"
      };
      return Axios(options)
        .then(response => {
          let data = response.data.data;
          return self.sortByKey(data, "tgl", "asc");
        })
        .catch(error => {
          console.log(error.response);
        });
    },
    getMoStatus(tgl) {
      var self = this;
      const options = {
        method: "GET",
        headers: {
          token: this.$store.getters.getToken
        },
        params: {
          tgl: tgl
        },
        url: this.$store.getters.getBaseURL + "/mostatus"
      };
      return Axios(options)
        .then(response => {
          let data = response.data.data;
          return self.sortByKey(data, "tgl", "asc");
        })
        .catch(error => {
          console.log(error.response);
        });
    },
    getCompleteness(tgl) {
      var self = this;
      const options = {
        method: "GET",
        headers: {
          token: this.$store.getters.getToken
        },
        params: {
          tgl: tgl
        },
        url: this.$store.getters.getBaseURL + "/completeness"
      };
      return Axios(options)
        .then(response => {
          let data = response.data.data;
          return self.sortByKey(data, "tgl", "asc");
        })
        .catch(error => {
          console.log(error.response);
        });
    },
    getMultisource(tgl) {
      var self = this;
      const options = {
        method: "GET",
        headers: {
          token: this.$store.getters.getToken
        },
        params: {
          tgl: tgl
        },
        url: this.$store.getters.getBaseURL + "/multisource"
      };
      return Axios(options)
        .then(response => {
          let data = response.data.data;
          return self.sortByKey(data, "tgl", "asc");
        })
        .catch(error => {
          console.log(error.response);
        });
    }
  }
});

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
